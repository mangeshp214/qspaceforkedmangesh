import React, {useState} from 'react';
import {View, TextInput, Text, StyleSheet} from 'react-native';

const TextInputLayout = props => {

    const [textInput, setTextInput] = useState('');
    const [title, setTitle] = useState('');
    
    const inputHandler = (enteredText) => {
        setTextInput(enteredText);
        props.setValue(enteredText);
        props.validate(enteredText);
    }

    return (

        <View style={props.style}>
            <Text style={styles.title}>{title}</Text>
            <View style={props.errorMessage == '' ? styles.inputBox : styles.errorInputBox}>
                <TextInput {...props} placeholder={title == '' ? props.placeholder : ''}
                onChangeText={inputHandler} 
                onFocus={() => setTitle(props.placeholder)}
                onBlur={() => setTitle('')}
                value={textInput}/>
            </View>
            <Text style={styles.errorText}>{props.errorMessage}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    inputBox: {
      borderColor: 'black',
      borderWidth: 1,
      borderRadius: 10,
      padding: 10,
      marginRight: 10
    },
    errorInputBox: {
        borderColor: 'red',
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        marginRight: 10
    },
    errorText: {
        color: 'red',
        paddingLeft: 10,
        fontSize: 12
    },
    title: {
        paddingLeft: 10,
        fontSize: 12
    }
});

export default TextInputLayout;