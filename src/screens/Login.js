// Homescreen.js
//import React, { Component } from 'react';
//import { StyleSheet, Button, View, Text } from 'react-native';

import React , {Component} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';

 export default class  Login extends Component {
  state = {
    email: "",
    password: ""
  }
  render() {
    return (
      <View style={styles.container}>
        
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Email..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ email: text })} />
        </View>
        <View style={styles.inputView} >
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ password: text })} />
        </View>

        <TouchableOpacity style={styles.loginBtn}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.SocialBtn}>
          <Text style={styles.SocialTextFB}>Facebook</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.SocialBtn}>
          <Text style={styles.SocialTextGoogle}>Google</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  SocialBtn: {
    borderWidth: 2,
    marginBottom: 10,
    borderRadius: 20,
    width: "60%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderColor: '#3b5998'

  },
  SocialTextFB: {
    color: "#3b5998"

  },
  SocialTextGoogle: {

  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    margin: 20
  },
  inputView: {
    width: "70%",
    borderBottomWidth: 1.0,
    height: 50,
    marginBottom: 30,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 60,
    color: "white"

  },
  forgot: {
    marginTop: 20,
    color: "black",
    fontSize: 13
  },
  loginBtn: {
    width: "60%",
    backgroundColor: "#00a1ff",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 30
  },
  loginText: {
    color: "white"
  }
});