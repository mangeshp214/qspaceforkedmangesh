import React, {useState} from 'react';
import {View, StyleSheet, Image, Text,TouchableNativeFeedback, Linking} from  'react-native';
import { Divider } from 'react-native-elements';
import { showLocation } from 'react-native-map-link';

import Card from '../components/Card';

const StoreDetailPage = props => {

    const[timeView, toggleTimeView] = useState(true);
    const[addressView, toggleAddressView] = useState(true);
    const[phoneView, togglePhoneView] = useState(true);

    var storeDetails = {
        storeName:'Sisodia Mini Market',
        openTime:'10:30 AM',
        closeTime:'8:30 PM',
        phoneNo:'+91 9665033576',
        latitude:38.8976763,
        longitude:-77.0387185,
        address: 'abc xyx this iss the address of our store. .lorem ipsum this is the addresss.lorem ipsum this is the addresss. lorem ipsum this '
    };
    const makeCall = (phoneNo) => {
        let phoneNumber = '';

        if (Platform.OS === 'android') {
        phoneNumber = 'tel:${'+phoneNo+'}';
        } else {
        phoneNumber = 'telprompt:${1234567890}';
        }

        Linking.openURL(phoneNumber);
    };

    const TimeView = timeView 
    ?
        <View style={styles.infoView}>
            <Image
                style={styles.logo}
                source={require('../images/time_trans.png')}
            />
            <Divider style={styles.dividerStyle}/>
            <View style={styles.innerInfoItem}>
                <Text>Opens <Text style={{fontSize:12, color:'red'}}>(Busy)</Text></Text>
                <Text style={{fontSize:24}}>{storeDetails.openTime}</Text>
                <Text>Closes</Text>
                <Text style={{fontSize:24}}>{storeDetails.closeTime}</Text>
            </View>
        </View>
    : 
        <View style={styles.infoView}>
            <Image
                style={styles.logo}
                source={require('../images/time_trans_toggle.png')}
            />
            <Divider style={styles.dividerStyle}/>
            <View style={styles.innerInfoItem}>
                <Text style={{fontSize:24}}>Single tap to view more details</Text>
            </View>
        </View>

    const AddressView = addressView
    ?
        <View style={styles.infoView}>
            <Image
                style={styles.logo}
                source={require('../images/location_trans.png')}
            />

            <Divider style={styles.dividerStyle}/>

            <View style={styles.innerInfoItem}>
                <Text style={{fontSize:18}}>{storeDetails.address}</Text>
            </View>
        </View>
    :
        <View style={styles.infoView}>
            <Image
                style={styles.logo}
                source={require('../images/location_trans_toggle.png')}
            />

            <Divider style={styles.dividerStyle}/>

            <View style={styles.innerInfoItem}>
                <Text style={{fontSize:24}}>Single tap to open the address in Google maps.</Text>
            </View>
        </View>

    const PhoneView = phoneView 
    ?
        <View style={styles.infoView}>
            <Image
                style={styles.logo}
                source={require('../images/phone_trans.png')}
            />

            <Divider style={styles.dividerStyle}/>

            <View style={styles.innerInfoItem}>
                <Text style={{fontSize:24}}>{storeDetails.phoneNo}</Text>
            </View>
        </View>
    :
        <View style={styles.infoView}>
            <Image
                style={styles.logo}
                source={require('../images/phone_trans_toggle.png')}
            />

            <Divider style={styles.dividerStyle}/>

            <View style={styles.innerInfoItem}>
                <Text style={{fontSize:24}}>Single tap to open the phone number in dialer app.</Text>
            </View>
        </View>
    
    return (
        <View style={styles.screen}>
            <Card style={styles.headerCard}>
                <View style={styles.headerView}>
                    <Text style={styles.headerText}>{storeDetails.storeName}</Text>
                    <Image
                        style={styles.headerImage}
                        source={require('../images/store_default.png')}>
                    </Image>
                </View>
            </Card>
            
            <Card style={styles.infoCard}>
                <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#c1b8ff', true)} onLongPress={toggle => toggleTimeView(false)} onPressOut={toggle => toggleTimeView(true)}>
                    <View>
                    {TimeView}
                    </View>
                </TouchableNativeFeedback>
            </Card>
            
            <Card style={styles.infoCard}>
                <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#c1b8ff', true)}  
                onPress={location => showLocation({latitude: storeDetails.latitude, longitude: storeDetails.longitude})}
                onLongPress={toggle => toggleAddressView(false)} onPressOut={toggle => toggleAddressView(true)}>
                    <View>
                        {AddressView}
                    </View>
                </TouchableNativeFeedback>
            </Card>
            <Card style={styles.infoCard}>
                <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple('#c1b8ff', true)} 
                onPress={call => makeCall(storeDetails.phoneNo)}
                onLongPress={toggle => togglePhoneView(false)} onPressOut={toggle => togglePhoneView(true)}>
                    <View>
                        {PhoneView}
                    </View>
                </TouchableNativeFeedback>
            </Card>
        </View>
    );

};

const styles = StyleSheet.create({
    headerCard: {
        height : '30%',
        backgroundColor: '#6B53FF'
    },
    screen : {
        flex:1
    },
    logo:{
        margin:10,
        height:100,
        width:100
    },
    infoCard : {
        padding:0,
        margin:10,
        marginVertical:5,
        borderRadius:20,
        elevation:4
    },
    infoView:{
        padding:10,
        flexDirection: 'row',
        justifyContent:'flex-start',
        alignItems:'center',
    },
    innerInfoItem : {
        justifyContent:'space-evenly',
        marginHorizontal:20,
        marginVertical: 20,
        flexWrap:'nowrap',
        flex:3
    },
    headerImage:{
        flex:5, 
        opacity: 0.8, 
        resizeMode:'contain', 
        width:'100%', 
        marginTop:20,
        height:'10%'
    },
    headerText:{
        flex: 1, 
        fontSize:32, 
        fontWeight:'bold', 
        color:'white',
        margin:10
    },
    headerView: {
        flex:1, 
        flexDirection:'column-reverse', 
        justifyContent:'space-between', 
        alignItems:'stretch'
    },
    dividerStyle:{
        backgroundColor:'grey', 
        width:1, 
        height:'85%', 
        margin:10, 
        opacity:0.5, 
        elevation:2
    }
});

export default StoreDetailPage;


